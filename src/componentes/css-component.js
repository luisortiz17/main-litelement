import { LitElement, html, css, unsafeCSS} from 'lit-element';

class CssComponent  extends LitElement {
    static get properties(){
        return {
            redStyle: {type:String}
        }
    }

  static get styles() {
    return css`
      div {
        color: ${unsafeCSS(this.redStyle)};
      }
    `;
  }
  

  static get properties() {
    return {};
  }

  constructor() {
    super();
    this.redStyle = "red";
  }

  render() {
    return html`
      <div> Contenido establecido</div>
    `;
  }
}

customElements.define('css-component', CssComponent);