import { LitElement } from "lit-element";

class Compuesto1Component extends LitElement {

    headerTemplate(title) {
        return html `
        <p>${title}</p>
        `
    }

    bodyTemplate(contenido) {
        return html `
        <p>${contenido}</p>
        `
    }

    //Model

    static get properties(){
        return {
            title: {type:String},
            contenido: {type: String}
        };
    }
    

    //Controller
    constructor() {
        super();
        this.title = "TITULO";
        this.contenido = "CONTENIDO";
    }

    //View
    render() {
        return html`
            ${this.headerTemplate(this.title)}
            ${this.bodyTemplate(this.contenido)}
        `;
    }

    
}
customElements.define('compuesto-component', Compuesto1Component);