import { LitElement, html } from 'lit-element';

class SlotComponent extends LitElement {

    //Model
    static get properties() {
        return {
            message: { type: String },
            title: {type:String}
        }
    }

    //Controller
    constructor() {
        super();
        this.message = "Mensaje privado";
    }

    //View
    render() {
        return html`
            <div>
                <p>Texto de prueba </p>
                <slot name="medio"></slot>
                <p>Texto de prueba 1 </p>
                <slot name="final"></slot>
            </div>
            
        `;
    }
}
customElements.define('slot-component', SlotComponent);