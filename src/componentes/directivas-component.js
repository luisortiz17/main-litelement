import { LitElement, html, css } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';
import { styleMap } from 'lit-html/directives/style-map';

class DirectivasComponent  extends LitElement {

  static get styles() {
    return css`
      .myDiv {
          background-color: blue;
      }
      .otraClase {
          border: 1px solid red;
        }
    `;
  }

  static get properties() {
    return {
        classes: {type: Object},
        styles: {type: Object}
    };
  }

  constructor() {
    super();
    this.classes = {myDiv: true, otraClase: true}
    this.styles = {color: 'green', fontFamily: 'Roboto'}
  }

  render() {
    return html`
    <div class=${classMap(this.classes)} style=${styleMap(this.styles)}>Algo</div>
      
    `;
  }
}

customElements.define('directivas-component', DirectivasComponent);