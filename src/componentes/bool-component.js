import { LitElement, html } from 'lit-element';

class BoolComponent extends LitElement {

    //Model
    static get properties() {
        return {
            message1: { type: String },
            message2: {type:String},
            miBool: {type: Boolean}
        }
    }

    //Controller
    constructor() {
        super();
        this.message1 = "Mensaje privado";
        this.message2 = "Mensaje publico";
        this.miBool = false;
    }

    //View
    render() {
        return html`
            ${this.miBool ? 
                html`<div style="color:red">${this.message1}</div>` :
                html`<div style="color:green">${this.message2}</div>`}
        `;
    }
}
customElements.define('bool-component', BoolComponent);