import { LitElement, html } from 'lit-element';

class ButtonComponent extends LitElement {

    //Model
    static get properties() {
        return {
            message: { type: String }
        }
    }

    //Controller
    constructor() {
        super();
        this.message = "Mensaje privado";
    }

    //View
    render() {
        return html`
            <div>${this.message}</div>
            <div><button @click="${this.clickHandler}">Probar</button></div>
        `;
    }

    clickHandler(e){
        console.log(e.target);
        this.message = 'new message';
    }
}
customElements.define('button-component', ButtonComponent);