import { LitElement, html, css } from 'lit-element';

class AttrComponent  extends LitElement {


  static get properties() {
    return {
        areax: {attribute: true},
        areay: {attribute: true}
    };
  }

  constructor() {
    super();
    this.areax = 10;
    this.areay = 20;
  }

  attributeChangedCallback(name, oldval, newval){
      console.log("Atributo modificado: " + name + newval);
      super.attributeChangedCallback(name, oldval, newval);
  }

  render() {
    return html`
      <p>AreaX: ${this.areax} </p>
      <p>AreaY: ${this.areay} </p>
      <button @click="${this.changeAtributes}"> Cambiar attr desde dentro</button>
    `;
  }

  changeAtributes(){
      let rndval = Math.floor(Math.random() *100).toString();
      this.setAttribute('areax', rndval);
      this.setAttribute('areay', rndval);
  }

  //Se dispara en automatico cuando cambia el estado(un valor) del componente
  updated(changedProperties){
      changedProperties.forEach((oldValue, propName) =>{
          console.log(`${propName} modificada. Valor anterior ${oldValue}`);
      });
  }
}

customElements.define('attr-component', AttrComponent);