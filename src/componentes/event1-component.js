import { LitElement, html, css } from 'lit-element';
import { PropchComponent } from './propch-component.js'

class Event1Component  extends LitElement {


  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <propch-component @event-cambio="${(e) => { alert(e.detail.message);}}"></propch-component>
    `;
  }
}

customElements.define('event1-component', Event1Component);