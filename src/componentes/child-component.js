import { LitElement, html, css } from 'lit-element';
import { SuperComponent } from './super-component.js'

class ChildComponent  extends SuperComponent {

  static get styles() {
    return [
        super.styles,
        css`
      button {
        color: red;
      }
    `
    ] ;
  }

 
}

customElements.define('child-component', ChildComponent);