import { LitElement, html, css } from 'lit-element';

export class PropchComponent  extends LitElement {


  static get properties() {
    return {
        miprop: {type:Number,
                 hasChanged(newVal, oldVal){ //hasChanged es propia de Lit-Element
                     if(newVal > oldVal){
                         console.log(`${newVal} > ${oldVal}.hasChanged: true`);
                         return true;
                     }else{
                        console.log(`${newVal} <= ${oldVal}.hasChanged: false`);
                        return false;
                     }
                 }}
    };
  }

  constructor() {
    super();
    this.miprop = 1;
  }
  updated(){
      console.log('updated');
      let event = new CustomEvent('event-cambio', {
          detail: {
              message: "Ha cambiado el valor"
          }
      });
      this.dispatchEvent(event);
  }

  getNewVal(){
      let newVal = Math.floor(Math.random()*10);
      this.miprop = newVal;
  }

  render() {
    return html`
      <p>Mi Prop: ${this.miprop}</p>
      <button @click="${this.getNewVal}">Cambiar Prop</button>
    `;
  }
}

customElements.define('propch-component', PropchComponent);