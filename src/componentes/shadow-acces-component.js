import { LitElement, html } from 'lit-element';

class ShadowAccessComponent extends LitElement {

    //Model
    

    //Controller
    constructor() {
        super();
    }

    //View
    render() {
        return html`
            <div id="miDiv" style="width:100px; height:100px"></div>
            <div><button @click="${this.clickHandler}">Probar</button></div>
        `;
    }

    clickHandler(e){
        var miDiv = this.shadowRoot.querySelector('#miDiv');
        miDiv.style.backgroundColor = "red";
    }
}
customElements.define('shadow-component', ShadowAccessComponent);