import { LitElement, html } from 'lit-element';

class Saludo extends LitElement {

    //Model
    static get properties() {
        return {
            message: { type: String },
            title: {type:String}
        }
    }

    //Controller
    constructor() {
        super();
        this.message = "Mensaje privado";
    }

    //View
    render() {
        return html`
            <div>${this.message}</div>
            <h3>Hi Luis</h3>
        `;
    }
}
customElements.define('test-saludos', Saludo);