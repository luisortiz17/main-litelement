import { LitElement, html } from 'lit-element';

class ArrComponent extends LitElement {

    //Model
    static get properties() {
        return {
            navegadores: { type: Array }
        }
    }

    //Controller
    constructor() {
        super();
        this.navegadores = ["Chrome", "Safari", "Opera", "Firefox"]
    }

    //View
    render() {
        return html`
        
            <ul>
                ${this.navegadores.map(i => html`<li> ${i} </li>`)}
            </ul>
            
        `;
    }
}
customElements.define('arr-component', ArrComponent);