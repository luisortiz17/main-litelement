import { LitElement, html } from 'lit-element';
import { until } from 'lit-html/directives/until.js'

class UntilComponent extends LitElement {

    //Model
    static get properties() {
        return {
            message: { type: String }
        }
    }

    //Controller
    constructor() {
        super();
        this.content = fetch('ruta/archivo.txt').then(r => r.text());
    }

    //View
    render() {
        return html`
            ${until(this.content, html `<span> Cargando ...</span>`)}
        `;
    }

    
}
customElements.define('until-component', UntilComponent);